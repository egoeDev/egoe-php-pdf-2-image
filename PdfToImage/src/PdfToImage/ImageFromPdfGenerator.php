<?php

namespace PdfToImage;

class ImageFromPdfGenerator 
{
    /** Imagine filter */
    protected $imagineFilterSets = array();

    protected $mainImagePattern = "%s.png";

    public function __construct(array $imagineFilterSets) 
    {
        $this->imagineFilterSets = $imagineFilterSets;
    }

    /**
     * Image\vignette from given PDF file generator
     */
    public function generateAllImageFromPdf($pdfFilePath, $destinationFolder, $ratio=1)
    {
        for ($i = 0; $i<$this->getPageNumbers($pdfFilePath); $i++) {
            $this->createMainImageItem($pdfFilePath, $destinationFolder, $i);

            foreach ($this->imagineFilterSets as $filterSetName => $filterSetConf) {
                foreach ($filterSetConf['filters'] as $name => $config) {
                    $imagine = new \Imagine\Imagick\Imagine();
                    $theMode = strtoupper($name.'_'.$config['mode']);

                    $imagine
                        ->open($this->getMainImageItemTarget($destinationFolder, $i))
                        ->thumbnail(new \Imagine\Image\Box(
                                ($ratio && $filterSetConf['ratio'])?($ratio * $config['size'][1]):$config['size'][0], 
                                $config['size'][1]
                            ), constant('\Imagine\Image\ImageInterface::'.$theMode)
                        )
                        ->save($destinationFolder.DIRECTORY_SEPARATOR.sprintf($filterSetConf['name_pattern'], $i+1))
                    ;
                }
            }
        }
    }

    public function setMainImagePattern($mainImagePattern)
    {
        $this->mainImagePattern = $mainImagePattern;

        return $this;
    }

    public function getPageNumbers($pdfFilePath) 
    {
        $im = new \imagick($pdfFilePath);
        $imageNumber = $im->getNumberImages();
        $im->clear();
        $im->destroy();

        return $imageNumber;
    }

    protected function createMainImageItem($filePath, $destinationFolder, $imagePageIndex)
    {            
        $im = new \imagick();
        $im->setResolution(300, 300);
        $im->readimage(sprintf('%s[%s]', $filePath, $imagePageIndex));
        $im->setImageFormat('jpeg');
        $im->writeImage($this->getMainImageItemTarget($destinationFolder, $imagePageIndex));
        $im->clear();
        $im->destroy();
    }

    protected function getMainImageItemTarget($destinationFolder, $imagePageIndex)
    {
        return sprintf('%s/%s', $destinationFolder, sprintf($this->mainImagePattern, ($imagePageIndex + 1)));
    }
}