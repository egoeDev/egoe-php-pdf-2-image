<?php

$loader = require '../vendor/autoload.php';

$filters = array(
    'publication_number_big' => array(
        'quality'=> 100,
        'filters' => array('thumbnail' =>  array('size' => array(1115, 1443), 'mode' => 'outbound')),
        'name_pattern' => '%s-large.jpg',
        'ratio' => true
    ),
    'publication_number_normal' => array(
        'quality'=> 100,
        'filters' => array('thumbnail' =>  array('size' => array(500, 650), 'mode' => 'outbound')),
        'name_pattern' => '%s-thumb.jpg',
        'ratio' => true
    ),
    'publication_number_thumb' => array(
        'quality'=> 100,
        'filters' => array('thumbnail' =>  array('size' => array(77, 100), 'mode' => 'outbound')),
        'name_pattern' => '%s.jpg',
        'ratio' => true
    ),
    'publication_number_preview_thumb' => array(
        'quality'=> 100,
        'filters' => array('thumbnail' =>  array('size' => array(57, 74), 'mode' => 'outbound')),
        'name_pattern' => '%s-preview.jpg',
        'ratio' => false
    )
);

$imageTools = new \PdfToImage\ImageFromPdfGenerator($filters);

$pdfPath = '/home/ghaliano/dev/egoe/PdfToImage/Demo/public/sample/numbers.pdf';
$imageFolderDestination = '/home/ghaliano/dev/egoe/PdfToImage/Demo/public/pdf';

$imageTools = $imageTools->generateAllImageFromPdf(
    $pdfPath, 
    $imageFolderDestination
);